<?php
require_once('controllers/base_controller.php');

class WorkController extends BaseController
{
  function __construct()
  {
    $this->folder = 'views';
  }

  public function index()
  {
    $data = array(
      'work_name' => 'test_name',
      'starting_date' => '24/12/2019',
      'ending_date' =>'25/12/2019',
      'status' => 1
    );
    $this->render('index', $data);
  }

  public function error()
  {
    $this->render('error');
  }
}