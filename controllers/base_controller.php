<?php
class BaseController
{
  protected $folder; 
  
  // Hàm hiển thị kết quả ra cho người dùng.
  function render($file, $data = array())
  {
    $view_file = 'views/' . $this->folder . '/' . $file . '.php';
    if (is_file($view_file)) {
      // Nếu tồn tại file đó thì tạo ra các biến chứa giá trị truyền vào lúc gọi hàm
      extract($data);
      ob_start();
      require_once($view_file);
      $content = ob_get_clean();
      // hiển thị trang cho người dùng
      require_once('views/layouts/application.php');
    } else {
      // chuyển sang trang báo lỗi
      header('Location: index.php?controller=work&action=error');
    }
  }
}